import fetch from '@/utils/fetch'
export function selectPutawayPage(data) {
  return fetch({
    url: '/inboundPutaway/selectPutawayPage',
    method: 'post',
    data: data
  })
}
// 入库记录导出
export function exportData(data) {
  return fetch({
    url: '/inboundPutaway/exportINRData',
    method: 'post',
    data: data
  })
}
