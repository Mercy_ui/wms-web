import fetch from '@/utils/fetch'
export function selectInboundPage(data) {
  return fetch({
    url: '/inboundNotice/selectInboundPage',
    method: 'post',
    data: data
  })
}
export function exportData(data) {
  return fetch({
    url: '/inboundNotice/exportINData',
    method: 'post',
    data: data
  })
}
