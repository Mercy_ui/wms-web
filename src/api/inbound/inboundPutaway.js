import fetch from '@/utils/fetch'
export function inboundTaskQueryPage(data) {
  return fetch({
    url: '/inboundTask/queryPage',
    method: 'post',
    data: data
  })
}
export function inboundTaskPutAwayList(data) {
  return fetch({
    url: '/inboundTask/putwayList',
    method: 'post',
    data: data
  })
}
export function inboundTaskSeachDetail(data) {
  return fetch({
    url: '/inboundTask/seachDetail',
    method: 'post',
    data: data
  })
}
export function inboundTaskPutWay(data) {
  return fetch({
    url: '/inboundTask/inboundPutWay',
    method: 'post',
    data: data
  })
}
export function queryUsableLocation(data) {
  return fetch({
    url: '/inboundTask/queryArea',
    method: 'post',
    data: data
  })
}

// 生成二维码 /inboundTask/printBarcode
export function printBarcode(data) {
  return fetch({
    url: '/inboundTask/printBarcode',
    method: 'post',
    data: data
  })
}

