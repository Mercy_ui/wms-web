import fetch from '@/utils/fetch'

// 分页查询
export function queryPage(data) {
  return fetch({
    url: '/costElement/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function addCostElement(data) {
  return fetch({
    url: '/costElement/addCostElement',
    method: 'post',
    data
  })
}

// 编辑
export function updateCostElement(data) {
  return fetch({
    url: '/costElement/updateCostElement',
    method: 'post',
    data
  })
}

// 删除
export function deleteForBatch(data) {
  return fetch({
    url: '/costElement/deleteForBatch',
    method: 'post',
    data
  })
}
