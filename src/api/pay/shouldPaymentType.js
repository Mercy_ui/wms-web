import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listShouldPaymentType(data) {
  return fetch({
    url: '/shouldPaymentType/list',
    method: 'post',
    data
  })
}

/**
 * 新增应付单类型
 * @param data
 */
export function addShouldPaymentType(data) {
  return fetch({
    url: '/shouldPaymentType/add',
    method: 'post',
    data
  })
}

/**
 * 获取应收单类型
 * @param data
 */
export function getShouldPaymentType(id) {
  return fetch({
    url: '/shouldPaymentType/get/' + id,
    method: 'get'
  })
}

/**
 * 更新应付单类型
 * @param data
 */
export function updateShouldPaymentType(data) {
  return fetch({
    url: '/shouldPaymentType/update',
    method: 'put',
    data
  })
}

/**
 * 删除应付单类型
 * @param data
 */
export function deleteShouldPaymentType(data) {
  return fetch({
    url: '/shouldPaymentType/delete',
    method: 'delete',
    data
  })
}

/**
 * 获取所有应付单类型
 * @param data
 */
export function getAllShouldPaymentType() {
  return fetch({
    url: '/shouldPaymentType/getAll',
    method: 'get'
  })
}

/**
 * 根据业务类型获取应付单类型
 * @param data
 */
export function getByBusinessType(data) {
  return fetch({
    url: '/shouldPaymentType/getByBusinessType',
    method: 'post',
    data
  })
}
