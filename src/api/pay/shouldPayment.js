import fetch from '@/utils/fetch'
export function queryPage(data) {
  return fetch({
    url: '/shouldPayment/queryPage',
    method: 'post',
    data
  })
}

export function create(data) {
  return fetch({
    url: '/shouldPayment/addShouldPayment',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/shouldPayment/updateShouldPayment',
    method: 'post',
    data
  })
}

export function deletedForBatch(data) {
  return fetch({
    url: '/shouldPayment/deleteForBatch',
    method: 'post',
    data
  })
}

export function getShouldPaymentById(id) {
  return fetch({
    url: '/shouldPayment/getShouldPaymentById/' + id,
    method: 'get'
  })
}

export function confirmShouldPayment(data) {
  return fetch({
    url: '/shouldPayment/confirmShouldPayment',
    method: 'post',
    data
  })
}

// 计算总价
export function computerPrice(data) {
  return fetch({
    url: '/shouldPayment/computerPrice',
    method: 'post',
    data
  })
}
