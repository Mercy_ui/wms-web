import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listWriteOff(data) {
  return fetch({
    url: '/writeOff/list',
    method: 'post',
    data
  })
}

/**
 * 新增核销单类型
 * @param data
 */
export function addWriteOff(data) {
  return fetch({
    url: '/writeOff/add',
    method: 'post',
    data
  })
}

/**
 * 获取核销单类型
 * @param data
 */
export function getWriteOff(id) {
  return fetch({
    url: '/writeOff/get/' + id,
    method: 'get'
  })
}

/**
 * 更新核销单类型
 * @param data
 */
export function updateWriteOff(data) {
  return fetch({
    url: '/writeOff/update',
    method: 'put',
    data
  })
}

/**
 * 删除核销单类型
 * @param data
 */
export function deleteWriteOff(data) {
  return fetch({
    url: '/writeOff/delete',
    method: 'delete',
    data
  })
}

/**
 * 审核付款单
 * @param data
 */
export function auditWriteOff(data) {
  return fetch({
    url: '/writeOff/audit',
    method: 'post',
    data
  })
}
