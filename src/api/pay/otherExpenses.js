import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function importExperses(header, rows) {
  const data = {
    header,
    rows
  }
  return fetch({
    url: '/otherExpenses/importData',
    method: 'post',
    data
  })
}
