import fetch from '@/utils/fetch'

// 指令查询
export function getShipmentList(data) {
  return fetch({
    url: '/OpManagement/getShipmentList',
    method: 'post',
    data: data
  })
}
