import fetch from '@/utils/fetch'

// 任务查询
export function getTaskList(data) {
  return fetch({
    url: '/OpManagement/getTaskList',
    method: 'post',
    data: data
  })
}
