import fetch from '@/utils/fetch'

// 异常查询
export function getExceptionRegisterList(data) {
  return fetch({
    url: '/OpManagement/getExceptionList',
    method: 'post',
    data: data
  })
}
