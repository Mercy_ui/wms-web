import fetch from '@/utils/fetchNoTimeout'

// 手动抓取
export function fetchShouldReceiptInvoiceData(data) {
  return fetch({
    url: '/shouldReceipt/fetchShouldReceiptInvoiceData',
    method: 'post',
    data
  })
}
