import fetch from '@/utils/fetch'
import fetchLogin from '@/utils/fetchLogin'

var qs = require('qs')

export function login(data) {
  return fetchLogin({
    url: '/oauth/token',
    method: 'post',
    data
  })
}

export function getInfo() {
  return fetch({
    url: '/user/info',
    method: 'get'
  })
}

export function logout() {
  return fetch({
    url: '/user/logout',
    method: 'post'
  })
}

export function examinPassword(password) {
  return fetch({
    url: '/account/examinPassword',
    method: 'get',
    params: { password }
  })
}

export function updatePwd(password) {
  return fetch({
    url: '/account/updatePwd',
    method: 'post',
    data: qs.stringify({
      password
    })
  })
}
