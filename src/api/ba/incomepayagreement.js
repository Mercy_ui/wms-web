import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/incomePayAgreement/queryPage',
    method: 'post',
    data
  })
}

export function add(data) {
  return fetch({
    url: '/incomePayAgreement/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/incomePayAgreement/update',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return fetch({
    url: '/incomePayAgreement/deleteIncomePayAgreement',
    method: 'post',
    data
  })
}

