import fetch from '@/utils/fetch'
export function queryPage(data) {
  return fetch({
    url: '/stockGood/queryListForPage',
    method: 'post',
    data
  })
}

export function create(data) {
  return fetch({
    url: '/stockGood/addStockGood',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/stockGood/updateStockGood',
    method: 'post',
    data
  })
}

export function deleted(id) {
  return fetch({
    url: '/stockGood/delete',
    method: 'get',
    params: { id }
  })
}

export function deletedForBatch(data) {
  return fetch({
    url: '/stockGood/deleteForBatch',
    method: 'post',
    data
  })
}

export function getStockGoodById(id) {
  return fetch({
    url: '/stockGood/getStockGoodById/' + id,
    method: 'get'
  })
}
