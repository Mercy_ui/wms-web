import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/apSettingDetail/queryPage',
    method: 'post',
    data
  })
}

export function create(data) {
  return fetch({
    url: '/apSettingDetail/create',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/apSettingDetail/update',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return fetch({
    url: '/apSettingDetail/deleted',
    method: 'post',
    data
  })
}

export function queryHeadIdList() {
  return fetch({
    url: '/apSetting/queryHeadIdList',
    method: 'get'
  })
}
