import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/projectTable/queryPage',
    method: 'post',
    data
  })
}

// 新增
export function addProjectControlTable(data) {
  return fetch({
    url: '/projectTable/addProjectControlTable',
    method: 'post',
    data
  })
}

// 编辑
export function updateProjectControlTable(data) {
  return fetch({
    url: '/projectTable/updateProjectControlTable',
    method: 'post',
    data
  })
}

// 批量删除
export function deleteForBatch(data) {
  return fetch({
    url: '/projectTable/deleteForBatch',
    method: 'post',
    data
  })
}
