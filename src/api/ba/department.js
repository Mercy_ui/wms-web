import fetch from '@/utils/fetch'

/**
 *  列表
 * @param data
 */
export function listDepartment(data) {
  return fetch({
    url: '/department/list',
    method: 'post',
    data
  })
}

/**
 * 获取部门树
 * @param data
 */
export function getDepartmentTree() {
  return fetch({
    url: '/department/getDepartmentTree',
    method: 'get'
  })
}

/**
 * 新增部门
 * @param data
 */
export function addDepartment(data) {
  return fetch({
    url: '/department/add',
    method: 'post',
    data
  })
}

/**
 * 获取部门
 * @param data
 */
export function getDepartment(id) {
  return fetch({
    url: '/department/get/' + id,
    method: 'get'
  })
}

/**
 * 更新部门
 * @param data
 */
export function updateDepartment(data) {
  return fetch({
    url: '/department/update',
    method: 'put',
    data
  })
}

/**
 * 删除部门
 * @param data
 */
export function deleteDepartment(data) {
  return fetch({
    url: '/department/delete',
    method: 'delete',
    data
  })
}

/**
 * 获取所有部门
 */
export function getAllDepartment() {
  return fetch({
    url: '/department/getAll',
    method: 'get'
  })
}

