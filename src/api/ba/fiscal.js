import fetch from '@/utils/fetch'

export function queryPage(data) {
  return fetch({
    url: '/fiscalPeriod/queryPage',
    method: 'post',
    data
  })
}

export function add(data) {
  return fetch({
    url: '/fiscalPeriod/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return fetch({
    url: '/fiscalPeriod/update',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return fetch({
    url: '/fiscalPeriod/deleteFiscalPeriod',
    method: 'post',
    data
  })
}

export function computeDataRange(data) {
  return fetch({
    url: '/fiscalPeriod/computeDataRange',
    method: 'post',
    data
  })
}

export function getFiscalPeriod(id) {
  return fetch({
    url: '/fiscalPeriod/getFiscalPeriodById/' + id,
    method: 'get'
  })
}
