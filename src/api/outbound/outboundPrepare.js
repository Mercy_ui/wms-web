import fetch from '@/utils/fetch'

// 列表查询
export function getPreparationList(data) {
  return fetch({
    url: '/preparationManage/getPreparationList',
    method: 'post',
    data: data
  })
}
export function getPreparationDetail(data) {
  return fetch({
    url: '/preparationManage/getPreparationDetail',
    method: 'post',
    data: data
  })
}
export function createPreparation(data) {
  return fetch({
    url: '/preparationManage/createPreparation',
    method: 'post',
    data: data
  })
}
export function changePreparation(data) {
  return fetch({
    url: '/preparationManage/changePreparation',
    method: 'post',
    data: data
  })
}
