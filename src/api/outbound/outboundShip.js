import fetch from '@/utils/fetch'

// 列表查询
export function getOutboundShipList(data) {
  return fetch({
    url: '/outboundShipManage/getOutboundShipList',
    method: 'post',
    data: data
  })
}
export function outboundShipConfirm(data) {
  return fetch({
    url: '/outboundShipManage/outboundShipConfirm',
    method: 'post',
    data: data
  })
}
export function getOutShipInfoScan(data) {
  return fetch({
    url: '/outboundShipManage/getOutShipInfoScan',
    method: 'post',
    data: data
  })
}
export function confirmOutboundScan(data) {
  return fetch({
    url: '/outboundShipManage/confirmOutboundScan',
    method: 'post',
    data: data
  })
}
export function getUseAbleLocation(data) {
  return fetch({
    url: '/inboundTask/queryArea',
    method: 'post',
    data: data
  })
}
export function getOutboundShipInfoForQuit(data) {
  return fetch({
    url: '/outboundShipManage/getOutboundShipInfoForQuit',
    method: 'post',
    data: data
  })
}
export function updateOutboundShipQuit(data) {
  return fetch({
    url: '/outboundShipManage/updateOutboundShipQuit',
    method: 'post',
    data: data
  })
}
// 出库记录
export function queryOutShipLine(data) {
  return fetch({
    url: '/outboundShipManage/queryOutboundShip',
    method: 'post',
    data: data
  })
}
// 出库记录导出
export function exportData(data) {
  return fetch({
    url: '/outboundShipManage/exportOSData',
    method: 'post',
    data: data
  })
}
