import fetch from '@/utils/fetch'
export function queryPage(data) {
  return fetch({
    url: '/preSubject/queryPage',
    method: 'post',
    data
  })
}

export function addPreSubject(data) {
  return fetch({
    url: '/preSubject/addPreSubject',
    method: 'post',
    data
  })
}

export function updatePreSubject(data) {
  return fetch({
    url: '/preSubject/updatePreSubject',
    method: 'post',
    data
  })
}

export function deletePreSubject(data) {
  return fetch({
    url: '/preSubject/deletePreSubject',
    method: 'post',
    data
  })
}

export function addDetail(data) {
  return fetch({
    url: '/preSubject/addDetail',
    method: 'post',
    data
  })
}

export function getDetailByHeadId(headId) {
  return fetch({
    url: '/preSubject/getDetailByHeadId/' + headId,
    method: 'get'
  })
}

export function getDetailList(headId) {
  return fetch({
    url: '/preSubject/getDetailList/' + headId,
    method: 'get'
  })
}

export function deleteDetaliByIdList(data) {
  return fetch({
    url: '/preSubject/deleteDetaliByIdList',
    method: 'post',
    data
  })
}

export function getDetailById(id) {
  return fetch({
    url: '/preSubject/getDetailById/' + id,
    method: 'get'
  })
}

