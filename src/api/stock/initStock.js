import fetch from '@/utils/fetch'

// 导入excel
export function importStockInit(data) {
  return fetch({
    url: '/stockInit/importStockInit',
    method: 'post',
    data: data
  })
}
// 列表查询
export function getStockInitList(data) {
  return fetch({
    url: '/stockInit/getStockInitList',
    method: 'post',
    data: data
  })
}
