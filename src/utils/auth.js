import Cookies from 'js-cookie'

const TokenKey = 'Authorization'
const RefreshTokenKey = 'RefreshToken'

/**
 * 将过期时间秒转化为天数
 */
function s2d(s) {
  const a = parseInt(s)
  return a / (24 * 60 * 60)
}

export function getToken() {
  return Cookies.get(TokenKey)
}

/**
 * 注释
 * @param token    将要存储的token
 * @param t         过期时间（单位： 秒）
 * @returns {*}
 */
export function setToken(token, t) {
  return Cookies.set(TokenKey, token, { expires: s2d(6000) })
}

export function removeToken() {
  Cookies.remove(RefreshTokenKey)
  return Cookies.remove(TokenKey)
}

/**
 * 获取refreshToken
 * @returns {*}
 */
export function getRefreshToken() {
  return Cookies.get(RefreshTokenKey)
}

/**
 * 设置refreshToken
 * 默认过期时间 25天
 * @returns {*}
 */
export function setRefreshToken(token) {
  return Cookies.set(RefreshTokenKey, token, { expires: 25 })
}
