import Vue from 'vue'

// 付款单状态
Vue.filter('stockStatusFilter', (v) => {
  if (v === '10') {
    return '正常'
  } else if (v === '20') {
    return '锁定'
  }
})
Vue.filter('inboundLineStatusFilter', (v) => {
  if (v === '0') {
    return '不入库'
  } else if (v === '10') {
    return '未入库'
  } else if (v === '20') {
    return '部分入库'
  } else if (v === '30') {
    return '全部入库'
  } else if (v === '40') {
    return '关闭入库'
  } else if (v === '40') {
    return '取消'
  } else {
    return '未设置'
  }
})
Vue.filter('inboundInspectStatusFilter', (v) => {
  if (v === '10') {
    return '全部合格'
  } else if (v === '20') {
    return '部分合格'
  } else if (v === '30') {
    return '全部破损'
  } else if (v === '0') {
    return '未质检'
  } else if (v === null) {
    return '未质检'
  } else {
    return v
  }
})
Vue.filter('putawayGenMethodFilter', (v) => {
  if (v === '10') {
    return '手工创建'
  } else if (v === '20') {
    return '订单中心'
  } else if (v === '30') {
    return 'OTM'
  } else if (v === '40') {
    return 'APP'
  } else if (v === '50') {
    return '君马'
  } else if (v === '60') {
    return '扫码触发'
  } else if (v === '70') {
    return '道闸'
  } else {
    return v
  }
})
// 头出库状态
Vue.filter('noticeHeadStatusFilter', (v) => {
  if (v === '10') {
    return '未出库'
  } else if (v === '20') {
    return '部分出库'
  } else if (v === '30') {
    return '全部出库'
  } else if (v === '40') {
    return '关闭出库'
  } else if (v === '50') {
    return '取消出库'
  } else {
    return v
  }
})
// 明细出库状态
Vue.filter('noticeLineStatusFilter', (v) => {
  if (v === '0') {
    return '不出库'
  } else if (v === '10') {
    return '未出库'
  } else if (v === '20') {
    return '部分出库'
  } else if (v === '30') {
    return '全部出库'
  } else if (v === '40') {
    return '关闭出库'
  } else if (v === '50') {
    return '取消出库'
  } else {
    return v
  }
})
// 头备料状态
Vue.filter('prepareHeadStatusFilter', (v) => {
  if (v === '10') {
    return '未备料'
  } else if (v === '20') {
    return '开始执行'
  } else if (v === '30') {
    return '部分备料'
  } else if (v === '40') {
    return '全部完成'
  } else {
    return v
  }
})
// 明细备料状态
Vue.filter('prepareLineStatusFilter', (v) => {
  if (v === '10') {
    return '未开始'
  } else if (v === '20') {
    return '已开始'
  } else if (v === '30') {
    return '已完成'
  } else if (v === '40') {
    return '已取消'
  } else {
    return v
  }
})
// 异常分类
Vue.filter('exceptionTaskNodeFilter', (v) => {
  if (v === '0') {
    return '指令'
  } else if (v === '10') {
    return '寻车'
  } else if (v === '20') {
    return '移车'
  } else if (v === '30') {
    return '提车'
  } else if (v === '40') {
    return '收车质检'
  } else if (v === '41') {
    return '入库移车'
  } else if (v === '42') {
    return '分配入库'
  } else if (v === '50') {
    return '出库备车'
  } else if (v === '51') {
    return '出库确认'
  } else if (v === '60') {
    return '装车交验'
  } else {
    return v
  }
})
// 异常处理状态
Vue.filter('exceptionDealStatusFilter', (v) => {
  if (v === '10') {
    return '未处理'
  } else if (v === '20') {
    return '处理中'
  } else if (v === '30') {
    return '关闭'
  } else if (v === '40') {
    return '让步'
  } else {
    return v
  }
})
// 异常处理方式
Vue.filter('exceptionDealTypeFilter', (v) => {
  if (v === '10') {
    return '返厂维修'
  } else if (v === '20') {
    return '出库维修'
  } else if (v === '30') {
    return '库内维修'
  } else if (v === '40') {
    return '带伤发运'
  } else {
    return v
  }
})
// 任务类型
Vue.filter('taskTypeFilter', (v) => {
  if (v === '10') {
    return '寻车'
  } else if (v === '20') {
    return '移车'
  } else if (v === '30') {
    return '提车'
  } else {
    return v
  }
})
// 任务状态
Vue.filter('taskStatusFilter', (v) => {
  if (v === '10') {
    return '创建'
  } else if (v === '20') {
    return '开始'
  } else if (v === '30') {
    return '完成'
  } else if (v === '50') {
    return '取消'
  } else {
    return v
  }
})
// 运单状态
Vue.filter('orderReleaseStatusFilter', (v) => {
  if (v === '10') {
    return '正常'
  } else if (v === '50') {
    return '取消'
  } else {
    return v
  }
})
// 出入库类型
Vue.filter('boundTypeFilter', (v) => {
  if (v === '10') {
    return '入库'
  } else if (v === '20') {
    return '出库'
  } else if (v === '30') {
    return '移库'
  } else if (v === '40') {
    return '其他'
  } else {
    return v
  }
})

