import { GetSideNavs } from '@/api/navs'
import path from '@/router/path'

const navs = {
  state: {
    routers: [],
    addRouters: [],
    navmenus: []
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
    },
    SET_NullROUTERS: (state, routers) => {
      state.addRouters = routers
    },
    SET_NavMenus: (state, menus) => {
      state.navmenus = menus
    },
    SET_ALLROUTERS: (state, routers) => {
      state.routers = routers
    }
  },
  actions: {
    // 首次进入首页时，获取侧边菜单
    InitSideNavs({ commit }, pid) {
      return new Promise(resolve => {
        GetSideNavs(pid).then(response => {
          var filterRouters
          for (var i = 0; i < path.length; i++) {
            if (path[i].type === 1 && path[i].name === response.data.parent.name) {
              filterRouters = filterNavs(path[i].children, response.data.children)
            }
          }
          commit('SET_ROUTERS', filterRouters)
          resolve()
        })
      }).catch(error => {
        console.log(error)
      })
    },
    InitRouter({ commit }) {
      return new Promise(resolve => {
        var pid = 0
        GetSideNavs(pid).then(response => {
          var filterRouters = filterNavs(path, response.data.children)
          commit('SET_ALLROUTERS', filterRouters)
          resolve()
        })
      }).catch(error => {
        console.log(error)
      })
    },
    // 点击顶部菜单时，获取侧边菜单
    GenerateSideNavsByTopNav({ commit }, pid) {
      return new Promise(resolve => {
        GetSideNavs(pid).then(response => {
          var filterRouters
          for (var i = 0; i < path.length; i++) {
            if (path[i].type === 1 && path[i].name === response.data.parent.name) {
              filterRouters = filterNavs(path[i].children, response.data.children)
            }
          }
          commit('SET_ROUTERS', filterRouters)
          resolve()
        })
      }).catch(error => {
        console.log(error)
      })
    },
    ClearRouters({ commit }) {
      commit('SET_NullROUTERS', [])
    },
    GetTopMenus({ commit }, menus) {
      commit('SET_NavMenus', menus)
    }
  }
}

/**
 *
 * @param navs 前端维护的路由全集
 * @param perms 后端返回的可访问菜单（仅包含名称）
 */
function filterNavs(navs, perms) {
  const accessableRouters = navs.filter(route => {
    if (route.type === 1) {
      route.children = route.children
      filterNavs(route.children, perms)
      return true
    } else {
      if (hasPermissions(route, perms)) {
        if (route.children && route.children.length) {
          route.children = filterNavs(route.children, perms)
        }
        return true
      }
      return false
    }
  })
  return accessableRouters
}

/**
 *
 * @param nav 前端维护的路由（单条）
 * @param perms（后端返回的可访问菜单）
 * @returns {boolean}
 */

function hasPermissions(nav, perms) {
  if (nav && nav.name) {
    // return perms.some(perm => nav.name.indexOf(perm.name) >= 0)
    return perms.some(perm => nav.name === perm.name)
  } else {
    return true
  }
}

export default navs
