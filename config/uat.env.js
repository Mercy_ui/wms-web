var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API: '"http://113.105.88.142:8580/wms-admin"',
  BASE_API_LOGIN:'"https://lisa-test.huiyunche.cn/uaa"'
})
