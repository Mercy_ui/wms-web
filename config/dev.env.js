var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // BASE_API: '"http://113.105.88.142:8580/wms-admin"',
  // BASE_API: '"http://10.20.30.111/wms-admin/"',
  // BASE_API: '"http://localhost:8090"',
  // BASE_API: '"http://10.2.4.40:8090/"',
  // BASE_API: '"http://10.2.6.12:8090"',
  // BASE_API: '"http://10.2.6.6:8090"',
  // BASE_API: '"http://10.2.6.250:8090/"',
  // BASE_API_LOGIN:'"http://10.20.30.111:8090/uaa"'
  BASE_API: '"http://113.105.88.142:8580/wms-admin"',
  BASE_API_LOGIN:'"https://lisa-test.huiyunche.cn/uaa"'
})
